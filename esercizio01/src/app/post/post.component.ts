import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/api.service';
import { Post } from '../_modules/post';
import { filter, switchMap } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  posts: Post[]=[];


  constructor(private apiService: ApiService) { }

  ngOnInit() {
/*
    // estrai i body dei post

    this.apiService.getPost().subscribe(
      post => this.posts = (<Post[]> post)
    );
*/

    // estrai id, titolo e body dei post con id pari

    this.apiService.getPost().pipe(
      switchMap((post: any) => from(post)),
      filter((post: any) => (post.id % 2 == 0)
      )).subscribe((post: any) => {
        console.log(post.id);
        console.log(post.title);
        console.log(post.body);

        this.posts.push(post);
      });

  }
}


/*





*/
