import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  getPost(){
    // ritorna un observer
    return this.http.get("https://jsonplaceholder.typicode.com/posts");
  }

}
